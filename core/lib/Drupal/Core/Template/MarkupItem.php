<?php

/**
 * @file
 * Definition of Drupal\Core\Template\MarkupItem.
 */

namespace Drupal\Core\Template;
use Iterator;

class MarkupItem implements Iterator {
  public $attributes = array();
  protected $content, $key, $value, $valid;
  function __construct($content, $attributes = array()) {
    $this->attributes = new Attribute($attributes);
    $this->content = $content;
    if (is_array($this->content)) {
      $this->rewind();
    }
  }
  function current() {
    return $this->value;
  }
  function key() {
    return $this->key;
  }
  function next() {
    $this->valid = list($this->key, $this->value) = each($this->content);
  }
  function rewind() {
    reset($this->content);
    $this->next();
  }
  function valid() {
    return $this->valid !== FALSE;
  }
  function __toString() {
    return $this->content;
  }
}
