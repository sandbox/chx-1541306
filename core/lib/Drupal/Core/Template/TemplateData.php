<?php

/**
 * @file
 * Definition of Drupal\Core\Template\Template.
 */

namespace Drupal\Core\Template;
use Twig_Markup;
use ArrayAccess;

class TemplateData extends Twig_Markup implements ArrayAccess {
  protected $context, $type;

  function __construct($type, $context = array()) {
    $context += array('attributes' => array());
    $this->type = $type;
    // @TODO: remove this when drupal_render() is gone.
    $context['#defaults_loaded'] = TRUE;
    $this->context = $context;
    // @TODO: remove this when drupal_render() is gone.
    $this->context['#pre_render'] = array('Drupal\Core\Template\drupal_render');
  }

  function &offsetGet($offset) {
    return $this->context[$offset];
  }

  function offsetSet($offset, $context) {
    $this->context[$offset] = $context;
  }

  function offsetUnset($offset) {
    unset($this->context[$offset]);
  }

  function offsetExists($offset) {
    return isset($this->context[$offset]);
  }

  function getContext() {
    return $this->context;
  }

  function getType() {
    return $this->type;
  }

  function render() {
    #preprocess($this->type, $this->context);
    $template = twig()->loadTemplate(drupal_get_path('theme', $GLOBALS['theme']) . '/' . $this->type . '.twig');
    return $template->render($this->context);
  }

  function __toString() {
    // Make sure we do NOT return a string so that we get a pretty error
    // if this function gets called.
  }
}

/**
 * Render TemplateData objects when they are inside a render array().
 * Note: this is a namespaced function.
 * @TODO: remove this when drupal_render() is gone.
 */
function drupal_render($elements) {
  $elements['#children'] = $elements->render();
  return $elements;
}
