<?php

/**
 * @file
 * Definition of Drupal\Core\Template\TwigNodeExpressionName.
 */

namespace Drupal\Core\Template;
use Twig_Node_Expression_Name;
use Twig_Compiler;

class TwigNodeExpressionName extends Twig_Node_Expression_Name {
    public function compile(Twig_Compiler $compiler)
    {
        $name = $this->getAttribute('name');
        if ($this->getAttribute('is_defined_test')) {
            if ($this->isSpecial()) {
                $compiler->repr(true);
            } else {
                $compiler->raw('array_key_exists(')->repr($name)->raw(', $context)');
            }
        } elseif ($this->isSpecial()) {
            $compiler->raw($this->specialVars[$name]);
        } else {
            // remove the non-PHP 5.4 version when PHP 5.3 support is dropped
            // as the non-optimized version is just a workaround for slow ternary operator
            // when the context has a lot of variables
/*            if (version_compare(phpversion(), '5.4.0RC1', '>=') && ($this->getAttribute('ignore_strict_check') || !$compiler->getEnvironment()->isStrictVariables())) {
                // PHP 5.4 ternary operator performance was optimized
                $compiler
                    ->raw('isset($context[')
                    ->string($name)
                    ->raw(']) ? $context[')
                    ->string($name)
                    ->raw('] : ($context[')
                    ->string($name)
                    ->raw('] = drupal_get_template_variable($context, ')
                    ->string($name)
                    ->raw('))')
                 ;
            } else
*/
            {
                $compiler
                    ->raw('$this->getDrupalContext($context, ')
                    ->string($name)
                ;

                if ($this->getAttribute('ignore_strict_check')) {
                    $compiler->raw(', true');
                }

                $compiler
                    ->raw(')')
                ;
            }
        }
    }
}
