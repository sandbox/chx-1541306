<?php

/**
 * @file
 * Definition of Drupal\Core\Template\TwigNodeVisitor.
 */

namespace Drupal\Core\Template;
use Twig_NodeVisitorInterface;
use Twig_NodeInterface;
use Twig_Environment;
use Twig_Node_Print;
use Twig_Node;
use Twig_Node_Expression_Function;

class TwigNodeVisitor implements Twig_NodeVisitorInterface {
  function enterNode(Twig_NodeInterface $node, Twig_Environment $env) {
    if (get_class($node) == 'Twig_Node_Expression_Name') {
      return new TwigNodeExpressionName($node->getAttribute('name'), $node->getLine());
    }
    return $node;
  }
  function leaveNode(Twig_NodeInterface $node, Twig_Environment $env) {
    if ($node instanceof Twig_Node_Print) {
      $class = get_class($node);
      return new $class(
        new Twig_Node_Expression_Function('myrender', new Twig_Node(array($node->getNode('expr'))), $node->getLine()),
        $node->getLine()
      );
    }
    return $node;
  }
  function getPriority() {
    return 0;
  }
}
