<?php

/**
 * @file
 * Definition of Drupal\Core\Template\TwigTemplate.
 */

namespace Drupal\Core\Template;
use Twig_Template;
use Twig_Error_Runtime;

abstract class TwigTemplate extends Twig_Template {
    protected function getDrupalContext($context, $item, $ignoreStrictCheck = false)
    {
        if (!array_key_exists($item, $context)) {
          $context[$item] = drupal_get_template_variable($context, $item);
        }
        if (!array_key_exists($item, $context)) {
            if ($ignoreStrictCheck || !$this->env->isStrictVariables()) {
                return null;
            }

            throw new Twig_Error_Runtime(sprintf('Variable "%s" does not exist', $item));
        }

        return $context[$item];
    }

}
