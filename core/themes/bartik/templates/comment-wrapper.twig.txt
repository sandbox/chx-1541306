{#
/**
 * @file
 * Bartik's theme implementation to provide an HTML container for comments.
 *
 * Available variables:
 * - content.comments: Rendered output of comment list, from comment.twig.
 * - content.comment_form: Form for adding a new comment.
 * - attributes: Remaining HTML attributes for the containing element.
 * - attributes.id: A valid HTML ID and guaranteed unique.
 * - attributes.class: Classes that can be used to style contextually through
 *   CSS. The default classes include the following:
 *   - comment-wrapper: The current template type, i.e., "theming hook".
 * - title_prefix: An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - title_suffix: An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * The following variables are provided for contextual information.
 * - node: Node entity the comments are attached to.
 * - node.type: The type of the node.
 *
 * The constants below the variables show the possible values and should be
 * used for comparison.
 * - $display_mode
 *   - COMMENT_MODE_FLAT
 *   - COMMENT_MODE_THREADED
 *
 * @see template_preprocess_comment_wrapper()
 *
 * @ingroup themeable
 */
#}
<section id="comments" class="{{ attributes.class }}" {{- attributes }}>
  {# TODO forum module should override comment template to keep this clean #}
  {% if comments %}
    {% if node.type != 'forum' %}
      {{ render(title_prefix) }}
      <h2 class="title">{{ t('Comments') }}</h2>
      {{ render(title_suffix) }}
    {% endif %}
  {% endif %}

  {{ render(content.comments) }}

  {% if content.comment_form %}
    <h2 class="title comment-form">{{ t('Add new comment') }}</h2>
    {{ render(content.comment_form) }}
  {% endif %}
</section>