{#
/**
 * @file
 * Bartik's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head, and body tags are not in this template. Instead
 * they can be found in the html.tpl.php template normally located in the
 * core/modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on
 *   the menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node entity, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.featured: Items for the featured region.
 * - page.highlighted: Items for the highlighted content region.
 * - page.help: Dynamic help text, mostly for admin pages.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.triptych_first: Items for the first triptych.
 * - page.triptych_middle: Items for the middle triptych.
 * - page.triptych_last: Items for the last triptych.
 * - page.footer_firstcolumn: Items for the first footer column.
 * - page.footer_secondcolumn: Items for the second footer column.
 * - page.footer_thirdcolumn: Items for the third footer column.
 * - page.footer_fourthcolumn: Items for the fourth footer column.
 * - page.footer: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */

TODO: move ID's out of tags, via attributes.
TODO: I've bulk updated most of the updates int he lower 1/3 of the
file, need to sanitize and fix.
#}
<div id="page-wrapper"><div id="page">
  <div id="header" class="{{ secondary_menu ? 'with-secondary-menu': 'without-secondary-menu' }}"><div class="section clearfix">

    {% if (logo) %}
      <a href="{{ front_page }}" title="{{ t('Home') }}" rel="home" id="logo">
        <img src="{{ logo }}" alt="{{ t('Home') }}" />
      </a>
    {% endif %}

    {% if (site_name or site_slogan) %}
    {# TODO: refactor conditional element-invisible logic out of template #}
    {# <?php if ($hide_site_name && $hide_site_slogan) { print ' class="element-invisible"'; } #}
    <div id="name-and-slogan" {{ attributes }}>
      	{% if site_name %}
	  {% if (title) %}
            {# TODO: refactor conditional attribute logic: #}
            {# if ($hide_site_name) { print ' class="element-invisible"'; } #}
            <div id="site-name" {{ attributes }}>
              <strong>
                <a href="{{ front_page }}" title="{{ t('Home') }}" rel="home"><span>{{ site_name }}</span></a>
              </strong>
            </div>
	  {% else %} {# Use h1 when the content title is empty #}
            {# TODO: refactor conditional attribute logic: #}
            {# if ($hide_site_name) { print ' class="element-invisible"'; } #}
            <h1 id="site-name" {{ attributes }}>
              <a href="{{ front_page }}" title="{{ t('Home') }}" rel="home"><span>{{ site_name }}</span></a>
            </h1>
	  {% endif %}
	{% endif %}

        {% if site_slogan %}
          {# TODO: refactor conditional attribute logic: #}
          {# if ($hide_site_name) { print ' class="element-invisible"'; } #}
          <div id="site-slogan" {{ attributes }}>
            {{ site_slogan }}
          </div>
	{% endif %}

      </div> <!-- /#name-and-slogan -->
    {% endif %}

    {#
    TODO: This was changed from array to object via Twig ".".
    Confirm that works.
    #}
    {{ render(page.header) }}

    {% if main_menu %}
     <div id="main-menu" class="navigation">
       {{ main_menu }}
      </div> <!-- /#main-menu -->
    {% endif %}

    {% if secondary_menu %}
     <div id="secondary-menu" class="navigation">
       {{ secondary_menu }}
      </div> <!-- /#secondary-menu -->
    {% endif %}

  </div></div> <!-- /.section, /#header -->

  {% if (messages) %}
    <div id="messages"><div class="section clearfix">
      {{ messages }}
    </div></div> <!-- /.section, /#messages -->
  {% endif %}

  {% if (page.featured) %}
    <div id="featured"><div class="section clearfix">
      {{ render(page.featured) }}
    </div></div> <!-- /.section, /#featured -->
  {% endif %}

  <div id="main-wrapper" class="clearfix"><div id="main" class="clearfix">

    {{ breadcrumb }}

    {% if (page.sidebar_first) %}
      <div id="sidebar-first" class="column sidebar"><div class="section">
        {{ render(page.sidebar_first) }}
      </div></div> <!-- /.section, /#sidebar-first -->
    {% endif %}

    <div id="content" class="column"><div class="section">
      {% if (page.highlighted) %}<div id="highlighted">{{ render(page.highlighted) }}</div>{% endif %}
      <a id="main-content"></a>
      {{ render(title_prefix) }}
      {% if (title) %}
        <h1 class="title" id="page-title">
          {{ title }}
        </h1>
      {% endif %}
      {{ render(title_suffix) }}
      {% if (tabs) %}
        <div class="tabs">
          {{ render(tabs) }}
        </div>
      {% endif %}
      {{ render(page.help) }}
      {% if (action_links) %}
        <ul class="action-links">
          {{ render(action_links) }}
        </ul>
      {% endif %}
      {{ render(page.content) }}
      {{ feed_icons }}

    </div></div> <!-- /.section, /#content -->

    {% if (page.sidebar_second) %}
      <div id="sidebar-second" class="column sidebar"><div class="section">
        {{ render(page.sidebar_second) }}
      </div></div> <!-- /.section, /#sidebar-second -->
    {% endif %}

  </div></div> <!-- /#main, /#main-wrapper -->

  {% if (page.triptych_first or page.triptych_middle or page.triptych_last) %}
    <div id="triptych-wrapper"><div id="triptych" class="clearfix">
      {{ render(page.triptych_first) }}
      {{ render(page.triptych_middle) }}
      {{ render(page.triptych_last) }}
    </div></div> <!-- /#triptych, /#triptych-wrapper -->
  {% endif %}

  <div id="footer-wrapper"><div class="section">

    {% if (page.footer_firstcolumn or page.footer_secondcolumn or page.footer_thirdcolumn or page.footer_fourthcolumn) %}
      <div id="footer-columns" class="clearfix">
        {{ render(page.footer_firstcolumn) }}
        {{ render(page.footer_secondcolumn) }}
        {{ render(page.footer_thirdcolumn) }}
        {{ render(page.footer_fourthcolumn) }}
      </div> <!-- /#footer-columns -->
    {% endif %}

    {% if (page.footer) %}
      <div id="footer" class="clearfix">
        {{ render(page.footer) }}
      </div> <!-- /#footer -->
    {% endif %}

  </div></div> <!-- /.section, /#footer-wrapper -->

</div></div> <!-- /#page, /#page-wrapper -->
